//
//  MainDiffableDataSource.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

import UIKit

class MainDiffableDataSource {
    
    // MARK: - Public Properties
    var dataSource: UICollectionViewDiffableDataSource<MainSection, MainRow>?
    var isEditCallback: ((Bool)->())?
    var deleteItemFromListCallback: ((Int)->())?
    var addItemToListCallback: (()->())?
    
    // MARK: - Private Methods
    // MARK: - createHeader
    private func createHeader() {
        let skillsHeaderRegistration = UICollectionView.SupplementaryRegistration
        <SkillsSectionHeader>(elementKind: UICollectionView.elementKindSectionHeader) { [weak self] (headerView, elementKind, indexPath) in
            
            headerView.editCallback = { disableEditor in
                self?.isEditCallback?(disableEditor)
            }
        }
        
        let infoHeaderRegistration = UICollectionView.SupplementaryRegistration
        <InfoSectionHeader>(elementKind: UICollectionView.elementKindSectionHeader) { (headerView, elementKind, indexPath) in }
        
        dataSource?.supplementaryViewProvider = {
            (collectionView, elementKind, indexPath) -> UICollectionReusableView? in
            
            guard let section = MainSection(rawValue: indexPath.section) else {
                return UICollectionReusableView()
            }
            
            switch section {
            case .skills:
                return collectionView.dequeueConfiguredReusableSupplementary(
                    using: skillsHeaderRegistration, for: indexPath)
                
            case .info:
                return collectionView.dequeueConfiguredReusableSupplementary(
                    using: infoHeaderRegistration, for: indexPath)
                
            default:
                return UICollectionReusableView()
            }
        }
    }
    
    // MARK: - enableEditMode
    private func enableEditMode() {
        guard var snapshot = dataSource?.snapshot() else {
            return
        }
        
        snapshot.appendItems([.skills(.editorItem)], toSection: .skills)
        dataSource?.apply(snapshot)
    }
    
    // MARK: - disableEditMode
    private func disableEditMode() {
        guard var snapshot = dataSource?.snapshot() else {
            return
        }
        
        snapshot.deleteItems([.skills(.editorItem)])
        dataSource?.apply(snapshot)
    }
}
// MARK: - MainDiffableDataSourceProtocol
extension MainDiffableDataSource: MainDiffableDataSourceProtocol {
    
    // MARK: - configure
    func configure(collectionView: UICollectionView) {
        dataSource = UICollectionViewDiffableDataSource<MainSection, MainRow>(collectionView: collectionView, cellProvider: { [weak self] (collectionView, indexPath, item) -> UICollectionViewCell? in
            
            switch item {
            case .profile(let profile):
                let cell = self?.createCell(cellType: ProfileCell.self, indexPath: indexPath, collectionView: collectionView)
                cell?.configure(with: profile)
                
                return cell
                
            case .skills(let category):
                
                switch category {
                case .skillItem(let skill):
                    let cell = self?.createCell(cellType: SkillsCell.self, indexPath: indexPath, collectionView: collectionView)
                    cell?.configure(with: skill)
                    cell?.deleteItemCallback = { deleteCell in
                        guard let index = collectionView.indexPath(for: deleteCell)?.item else {
                            return
                        }
                        
                        self?.deleteItemFromListCallback?(index)
                    }
                    
                    return cell
                    
                case .editorItem:
                    let cell = self?.createCell(cellType: EditCell.self, indexPath: indexPath, collectionView: collectionView)
                    cell?.addButtonTapped = { [weak self] in
                        self?.addItemToListCallback?()
                    }
                    
                    return cell
                }
                
                
            case .info(let info):
                let cell = self?.createCell(cellType: InfoCell.self, indexPath: indexPath, collectionView: collectionView)
                cell?.configure(with: info)
                
                return cell
            }
        })
        
        createHeader()
    }
    
    // MARK: - reloadData
    func reloadData(with data: MainModel?, disableEditor: Bool, isAnimating: Bool) {
        if let data = data {
            var snapshot = NSDiffableDataSourceSnapshot<MainSection, MainRow>()
            
            snapshot.appendSections([.profile])
            snapshot.appendItems([.profile(data.profile)], toSection: .profile)
            
            snapshot.appendSections([.skills])
            data.skills.forEach {
                snapshot.appendItems([.skills(.skillItem($0))], toSection: .skills)
            }
            
            snapshot.appendSections([.info])
            snapshot.appendItems([.info(data.info)], toSection: .info)
            
            dataSource?.apply(snapshot, animatingDifferences: isAnimating)
        }
       
        if disableEditor {
            disableEditMode()
        } else {
            enableEditMode()
        }
    }
}
