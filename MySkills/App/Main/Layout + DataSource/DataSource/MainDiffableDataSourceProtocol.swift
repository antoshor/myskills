//
//  MainDiffableDataSourceProtocol.swift
//  MySkills
//
//  Created by Mac Admin on 21.10.2023.
//

import UIKit

protocol DiffableDataSourceProtocol {
    func createCell<T: CellReuseIDProtocol>(cellType: T.Type, indexPath: IndexPath, collectionView: UICollectionView) -> T
}

extension DiffableDataSourceProtocol {
    func createCell<T: CellReuseIDProtocol>(cellType: T.Type, indexPath: IndexPath, collectionView: UICollectionView) -> T {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellType.reuseID, for: indexPath) as? T else {
            fatalError("Error \(cellType)")
        }
        
        return cell
    }
}

protocol MainDiffableDataSourceProtocol: DiffableDataSourceProtocol {
    
    var isEditCallback: ((Bool)->())? { get set }
    
    var deleteItemFromListCallback: ((Int)->())? { get set }
    
    var addItemToListCallback: (()->())? { get set }
    
    func configure(collectionView: UICollectionView)

    func reloadData(with data: MainModel?, disableEditor: Bool, isAnimating: Bool)
}
