//
//  InfoSectionHeader.swift
//  MySkills
//
//  Created by Mac Admin on 25.10.2023.
//


import UIKit

class InfoSectionHeader: UICollectionReusableView {
    
    // MARK: - UI Properties
    private lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.text = "О себе"
        title.font = UIFont(name: "SFProDisplay-Medium", size: 20)
        title.textAlignment = .left
        
        return title
    }()
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: 24),
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
    }
    
}
