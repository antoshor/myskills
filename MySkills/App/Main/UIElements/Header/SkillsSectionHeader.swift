//
//  SkillsSectionHeader.swift
//  MySkills
//
//  Created by Mac Admin on 25.10.2023.
//

import UIKit

class SkillsSectionHeader: UICollectionReusableView {
    
    // MARK: - UI Properties
    // MARK: - containerStackView
    private lazy var containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 10
        [titleLabel, editButton].forEach {
            stack.addArrangedSubview($0)
        }
        
        return stack
    }()
    
    // MARK: - titleLabel
    private lazy var titleLabel: UILabel = {
        let title = UILabel()
        title.text = "Мои навыки"
        title.font = UIFont(name: "SFProDisplay-Medium", size: 20)
        
        return title
    }()
    
    // MARK: - editButton
    private lazy var editButton: UIButton = {
        let button = UIButton()
        
        button.setImage(UIImage(named: "Edit"), for: .normal)
        button.setImage(UIImage(named: "Select"), for: .selected)
        button.addTarget(nil, action: #selector(editButtonTapped), for: .touchUpInside)
        
        return button
    }()

    // MARK: - Public Properties
    var editCallback: ((Bool)->())?
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - @objc Methods
    @objc func editButtonTapped(_ sender: UIButton) {
        if sender.isSelected {
            editCallback?(true)
            sender.isSelected = false
        } else {
            editCallback?(false)
            sender.isSelected = true
        }
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(containerStackView)
        
        NSLayoutConstraint.activate([
            containerStackView.topAnchor.constraint(equalTo: topAnchor, constant: 21),
            containerStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerStackView.bottomAnchor.constraint(equalTo: bottomAnchor),
        ])
        
        editButton.setContentCompressionResistancePriority(UILayoutPriority(749), for: .horizontal)
    }
}
