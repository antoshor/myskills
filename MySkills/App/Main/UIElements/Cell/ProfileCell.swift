//
//  ProfileCell.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

import UIKit

class ProfileCell: UICollectionViewCell {
    
    // MARK: - UI Properties
    // MARK: - containerStackView
    private lazy var containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 16
        stack.alignment = .center
        [imageView, infoStackViewContainer].forEach {
            stack.addArrangedSubview($0)
        }
        
        return stack
    }()
    
    // MARK: - infoStackViewContainer
    private lazy var infoStackViewContainer: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 4
        stack.alignment = .center
        [profileLabel, descriptionLabel, locationLabel].forEach {
            stack.addArrangedSubview($0)
        }
        
        return stack
    }()
    
    // MARK: - imageView
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        
        return image
    }()
    
    // MARK: - profileLabel
    private lazy var profileLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.font = UIFont(name: "SFProDisplay-Bold", size: 30)
        label.textAlignment = .center
        
        return label
    }()
    
    // MARK: - descriptionLabel
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont(name: "SFProDisplay-Medium", size: 18)
        label.textColor = #colorLiteral(red: 0.6537383795, green: 0.6508644223, blue: 0.6713317633, alpha: 1)
        label.textAlignment = .center
        
        return label
    }()
    
    // MARK: - locationLabel
    private lazy var locationLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 1
        label.font = UIFont(name: "SFProDisplay-Medium", size: 18)
        label.textColor = #colorLiteral(red: 0.6537383795, green: 0.6508644223, blue: 0.6713317633, alpha: 1)
        label.textAlignment = .center
        
        return label
    }()
    
    // MARK: - Private Properties
    private var check: Profile?
    private let queue = DispatchQueue(label: "queue.cell")
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = #colorLiteral(red: 0.9625456929, green: 0.9627652764, blue: 0.968842566, alpha: 1)
        setupConstraints()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - layoutSubviews
    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layer.cornerRadius = imageView.frame.height / 2
        imageView.clipsToBounds = true
    }
    
    // MARK: - prepareForReuse
    override func prepareForReuse() {
        imageView.image = nil
        profileLabel.text = nil
        descriptionLabel.text = nil
        locationLabel.text = nil
    }
    
    // MARK: - setupConstraints
    private func setupConstraints() {
        setupStackViewConstraints()
        setupImageViewConstraints()
    }
    
    // MARK: - setupStackViewConstraints
    private func setupStackViewConstraints() {
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(containerStackView)
        
        NSLayoutConstraint.activate([
            containerStackView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            containerStackView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            containerStackView.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor, constant: 24),
            containerStackView.leadingAnchor.constraint(greaterThanOrEqualTo: contentView.leadingAnchor, constant: 16),
            containerStackView.trailingAnchor.constraint(lessThanOrEqualTo: contentView.trailingAnchor, constant: -16),
            containerStackView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -24)
        ])
    }
    
    // MARK: - setupImageViewConstraints
    private func setupImageViewConstraints() {
        imageView.translatesAutoresizingMaskIntoConstraints = false
    
        NSLayoutConstraint.activate([
            imageView.widthAnchor.constraint(equalTo: imageView.heightAnchor, multiplier: 1),
            imageView.heightAnchor.constraint(equalToConstant: 150)
            
        ])
    }
}

// MARK: - CellProtocol
extension ProfileCell: CellConfigureProtocol {
    typealias CellData = Profile
    
    static var reuseID: String {
        return "ProfileCell"
    }
    
    func configure(with data: Profile) {
        check = data
        queue.async {
            // Simulation network loading image
            guard let image = UIImage(named: data.image), data == self.check else {
                print("Error with \(data)")
                return
            }
            
            DispatchQueue.main.async {
                self.imageView.image = image
            }
        }
        
        self.profileLabel.text = data.profileName
        self.descriptionLabel.text = data.description
        self.locationLabel.text = data.location
        self.layoutIfNeeded()
    }
}
