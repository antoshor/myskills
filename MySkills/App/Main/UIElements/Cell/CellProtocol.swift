//
//  CellProtocol.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

import UIKit

protocol CellReuseIDProtocol {
    
    static var reuseID: String { get }
}

protocol CellConfigureProtocol: CellReuseIDProtocol {

    associatedtype CellData
    
    func configure(with data: CellData)
}

