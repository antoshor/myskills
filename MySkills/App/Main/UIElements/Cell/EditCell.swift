//
//  EditCell.swift
//  MySkills
//
//  Created by Mac Admin on 26.10.2023.
//

import UIKit

class EditCell: UICollectionViewCell {
    
    // MARK: - UI Properties
    private var addItemButton: UIButton = {
        let button = UIButton()
        button.setTitle("+", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(nil, action: #selector(addItemButtonTapped), for: .touchUpInside)
        
        return button
    }()
    
    // MARK: - Public Properties
    var addButtonTapped: (()->())?
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = #colorLiteral(red: 0.9625456929, green: 0.9627652764, blue: 0.968842566, alpha: 1)
        contentView.layer.cornerRadius = 12
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - @objc Methods
    @objc func addItemButtonTapped(_ sender: UIButton) {
        addButtonTapped?()
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        addItemButton.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(addItemButton)
        
        NSLayoutConstraint.activate([
            addItemButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            addItemButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
            addItemButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            addItemButton.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12)
        ])
    }
}

// MARK: - CellProtocol
extension EditCell: CellReuseIDProtocol {

    static var reuseID: String {
        return "EditCell"
    }
}
