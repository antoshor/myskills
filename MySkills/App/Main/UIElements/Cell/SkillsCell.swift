//
//  SkillsCell.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

import UIKit

class SkillsCell: UICollectionViewCell {
    
    // MARK: - UI Properties
    // MARK: - containerStackView
    private lazy var containerStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.spacing = 10
        [titleLabel, deleteItemButton].forEach {
            stack.addArrangedSubview($0)
        }
        
        return stack
    }()
    
    // MARK: - titleLabel
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "SFProDisplay-Regular", size: 18)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 1
        
        return label
    }()
    
    // MARK: - deleteItemButton
    private lazy var deleteItemButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "closeButton"), for: .normal)
        button.addTarget(nil, action: #selector(deleteItem), for: .touchUpInside)
        
        return button
    }()
    
    
    // MARK: - Public Properties
    var deleteItemCallback: ((UICollectionViewCell)->())?
    
    // MARK: - Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
        setupContentView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - @objc Methods
    @objc func deleteItem(_ sender: UIButton) {
        deleteItemCallback?(self)
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        containerStackView.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(containerStackView)
        
        NSLayoutConstraint.activate([
            containerStackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 24),
            containerStackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -24),
            containerStackView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 12),
            containerStackView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -12)
        ])
    }
    
    
    private func setupContentView() {
        contentView.layer.cornerRadius = 12
        contentView.clipsToBounds = true
        contentView.backgroundColor = #colorLiteral(red: 0.9625456929, green: 0.9627652764, blue: 0.968842566, alpha: 1)
    }
}

// MARK: - CellProtocol
extension SkillsCell: CellConfigureProtocol {
    
    typealias CellData = Skills
    
    static var reuseID: String {
        return "SkillsCell"
    }
    
    func configure(with data: Skills) {
        deleteItemButton.isHidden = data.disableEditor
        titleLabel.text = data.skill
    }
}
