//
//  InfoCell.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

import UIKit

class InfoCell: UICollectionViewCell {
    
    // MARK: - UI Properties
    private lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont(name: "SFProDisplay-Regular", size: 18)
        label.numberOfLines = 0
        
        return label
    }()
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        descriptionLabel.text = nil
    }
    
    // MARK: - setupConstraints
    private func setupConstraints() {
        descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        contentView.addSubview(descriptionLabel)
        
        NSLayoutConstraint.activate([
            descriptionLabel.heightAnchor.constraint(greaterThanOrEqualToConstant: 30),
            
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 0),
            descriptionLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: 0),
            descriptionLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
            descriptionLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: 0)
        ])
    }
}

// MARK: - CellProtocol
extension InfoCell: CellConfigureProtocol {
    typealias CellData = Info
    
    static var reuseID: String {
        return "InfoCell"
    }
    
    func configure(with data: Info) {
        descriptionLabel.text = data.description
        descriptionLabel.sizeToFit()
    }
}
