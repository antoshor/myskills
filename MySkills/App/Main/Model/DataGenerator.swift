//
//  DataGenerator.swift
//  MySkills
//
//  Created by Mac Admin on 21.10.2023.
//

class DataGenerator {
    static func getData() -> MainModel {
        let profile = Profile(image: "Profile", profileName: "Красильников Антон Александрович", description: "Junior iOS-разработчик, опыт более 2-х лет", location: "Ижевск")
        
        let skills = [Skills(skill: "UIkit", disableEditor: true),
                      Skills(skill: "MVP", disableEditor: true),
                      Skills(skill: "CoreData", disableEditor: true),
                      Skills(skill: "Compositional Layout", disableEditor: true),
                      Skills(skill: "ООП и SOLID",  disableEditor: true)
        ]
        
        let info = Info(description: "Развиваюсь в программировании, учусь в магистратуре, занимаюсь спортом и люблю вкусно покушать.")
        
        return MainModel(profile: profile, skills: skills, info: info)
    }
}
