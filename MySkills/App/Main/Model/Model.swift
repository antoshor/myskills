//
//  MainModel.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

// MARK: - MainModel
struct MainModel {
    let profile: Profile
    var skills: [Skills]
    let info: Info
}

// MARK: - Profile
struct Profile: Hashable {
    let image: String
    let profileName: String
    let description: String
    let location: String
}

// MARK: - Skills
struct Skills: Hashable {
    let skill: String
    var disableEditor: Bool
}

// MARK: - Info
struct Info: Hashable {
    let description: String
}
 
// MARK: - MainSection
enum MainSection: Int {
    case profile
    case skills
    case info
}

// MARK: - MainRow
enum MainRow: Hashable {
    case profile(Profile)
    case skills(SkillItem)
    case info(Info)
}

// MARK: - SkillItem
enum SkillItem: Hashable {
    case skillItem(Skills)
    case editorItem
}
