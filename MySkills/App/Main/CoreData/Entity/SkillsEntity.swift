//
//  SkillsEntity.swift
//  MySkills
//
//  Created by Mac Admin on 31.10.2023.
//

import CoreData

class SkillsEntity: NSManagedObject {
    
    // MARK: - getAllSkills
    class func getAllSkills(context: NSManagedObjectContext) throws -> [SkillsEntity]? {
        let request: NSFetchRequest<SkillsEntity> = SkillsEntity.fetchRequest()
        
        do {
            let skills = try context.fetch(request)
            if !skills.isEmpty {
                return skills
            }
        } catch {
            throw error
        }
        
        return nil
    }
    
    // MARK: - find
    class func find(id: String, context: NSManagedObjectContext) throws -> SkillsEntity? {
        let request: NSFetchRequest<SkillsEntity> = SkillsEntity.fetchRequest()
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let skill = try context.fetch(request)
            if !skill.isEmpty {
                return skill[0]
            }
        } catch {
            throw error
        }
        
        return nil
    }
    
    // MARK: - create
    class func create(skills: [Skills], context: NSManagedObjectContext) {
        var skillsOutput = [SkillsEntity]()
        
        for value in skills {
            let skillsEntity = SkillsEntity(context: context)
            skillsEntity.skill = value.skill
            skillsEntity.id = "\(value.skill)"
            skillsOutput.append(skillsEntity)
        }
    }
    
    // MARK: - delete
    class func delete(id: String, context: NSManagedObjectContext) {
        guard let skill = try? SkillsEntity.find(id: id, context: context) else {
            print("Error: invalidated ID")
            return
        }
        context.delete(skill)
    }
}
