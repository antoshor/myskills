//
//  SkillsEntity+CoreDataProperties.swift
//  MySkills
//
//  Created by Mac Admin on 09.11.2023.
//
//

import Foundation
import CoreData


extension SkillsEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SkillsEntity> {
        return NSFetchRequest<SkillsEntity>(entityName: "SkillsEntity")
    }

    @NSManaged public var id: String
    @NSManaged public var skill: String
    @NSManaged public var profile: ProfileEntity

}

extension SkillsEntity : Identifiable {

}
