//
//  ProfileEntity.swift
//  MySkills
//
//  Created by Mac Admin on 31.10.2023.
//

import CoreData

class ProfileEntity: NSManagedObject {
    
    // MARK: - getProfile
    class func getProfile(context: NSManagedObjectContext) throws -> ProfileEntity? {
        let request: NSFetchRequest<ProfileEntity> = ProfileEntity.fetchRequest()
      
        do {
            let profile = try context.fetch(request)
            if !(profile.isEmpty) {
                return profile[0]
            }
        } catch {
            throw error
        }
        
        return nil
    }
    
    // MARK: - create
    class func create(id: String, profile: Profile, info: Info, context: NSManagedObjectContext) {
        let newProfile = ProfileEntity(context: context)
        newProfile.id = id
        newProfile.name = profile.profileName
        newProfile.profileInfo = profile.description
        newProfile.location = profile.location
        newProfile.profileDescription = info.description
    }
}

