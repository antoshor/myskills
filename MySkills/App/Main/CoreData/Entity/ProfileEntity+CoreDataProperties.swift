//
//  ProfileEntity+CoreDataProperties.swift
//  MySkills
//
//  Created by Mac Admin on 09.11.2023.
//
//

import Foundation
import CoreData


extension ProfileEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ProfileEntity> {
        return NSFetchRequest<ProfileEntity>(entityName: "ProfileEntity")
    }

    @NSManaged public var id: String
    @NSManaged public var location: String
    @NSManaged public var name: String
    @NSManaged public var profileDescription: String
    @NSManaged public var profileInfo: String
    @NSManaged public var skills: Set<SkillsEntity>

}

// MARK: Generated accessors for skills
extension ProfileEntity {

    @objc(addSkillsObject:)
    @NSManaged public func addToSkills(_ value: SkillsEntity)

    @objc(removeSkillsObject:)
    @NSManaged public func removeFromSkills(_ value: SkillsEntity)

    @objc(addSkills:)
    @NSManaged public func addToSkills(_ values: NSSet)

    @objc(removeSkills:)
    @NSManaged public func removeFromSkills(_ values: NSSet)

}

extension ProfileEntity : Identifiable {

}
