//
//  DataManager.swift
//  MySkills
//
//  Created by Mac Admin on 01.11.2023.
//

import CoreData

final class CoreDataManager {
    
    static let shared = CoreDataManager()
    
    // MARK: - Initializers
    private init() {}
    
    // MARK: - Public Methods
    // MARK: - getData
    func getData(_ completionHandler: @escaping (MainModel)->()) {
        guard let profileEntity = try? ProfileEntity.getProfile(context: AppDelegate.viewContext), let skillsEntity = try? SkillsEntity.getAllSkills(context: AppDelegate.viewContext) else {
            let data = DataGenerator.getData()
            AppDelegate.dbContainer.performBackgroundTask { context in
                
                ProfileEntity.create(id: "Profile", profile: data.profile, info: data.info, context: context)
                
                SkillsEntity.create(skills: data.skills, context: context)
                
                try? context.save()
                
                completionHandler(data)
            }
            return
        }
        
        var skills = [Skills]()
        skillsEntity.forEach {
            skills.append(Skills(skill: $0.skill, disableEditor: true))
        }
        
        let profile = Profile(image: "Profile", profileName: profileEntity.name, description: profileEntity.profileInfo, location: profileEntity.location)
        
        let info = Info(description: profileEntity.profileDescription)
        
        let output = MainModel(profile: profile, skills: skills, info: info)
        completionHandler(output)
    }
    
    // MARK: - deleteSkill
    func deleteSkill(id: String) {
        AppDelegate.dbContainer.performBackgroundTask { context in
            SkillsEntity.delete(id: id, context: context)
            try? context.save()
        }
    }
    
    // MARK: - appendSkill
    func appendSkill(skill: Skills) {
        AppDelegate.dbContainer.performBackgroundTask { context in
            SkillsEntity.create(skills: [skill], context: context)
            try? context.save()
        }
    }
}
