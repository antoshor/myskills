//
//  MainPresenter.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

import CoreData

class MainPresenter {
   
    // MARK: - Public Properties
    weak var view: MainViewProtocol?
    
    // MARK: - Private Methods
    private var data: MainModel?
    private var dataManager = CoreDataManager.shared
}

// MARK: - MainPresenterProtocol
extension MainPresenter: MainPresenterProtocol {
    
    // MARK: - initialLoad
    func initialLoad() {
      //  DispatchQueue.global().async {
        dataManager.getData { [weak self] data in
                self?.data = data
                DispatchQueue.main.async {
                    self?.view?.showUI(with: data, disableEditor: true, animating: false)
                }
            }
        //}
    }
    
    // MARK: - editSkillList
    func editSkillList(_ disableEditor: Bool) {
        if let myData = data {
            for i in 0..<myData.skills.count {
                self.data?.skills[i].disableEditor = disableEditor
            }
        }
        
        view?.reloadSkillList(with: data, disableEditor: disableEditor)
    }
    
    // MARK: - deleteItem
    func deleteItem(index: Int) {
        dataManager.deleteSkill(id: (data?.skills[index].skill)!)
        data?.skills.remove(at: index)
        view?.reloadSkillList(with: data!, disableEditor: false)
    }
    
    // MARK: - appendItem
    func appendItem(item: String?) {
        guard let item = item,
                let index = data?.skills.endIndex,
                !item.isEmpty else {
            return
        }
       
        let skill = Skills(skill: item, disableEditor: false)
        
        guard !(data?.skills.contains(skill))! else {
            view?.showDuplicateAlert(title: "Ошибка", subTitle: "Вы уже добавили этот навык", acceptAction: "Ок")
            return
        }
        
        data?.skills.insert(skill, at: index)
        dataManager.appendSkill(skill: Skills(skill: item, disableEditor: true))
        
        view?.reloadSkillList(with: data!, disableEditor: false)
    }
    
    // MARK: - showAlert
    func showAlert() {
        view?.showEditAlert(title: "Добавление навыка", subTitle: "Введите название навыка которым вы владеете", acceptAction: "Добавить", cancelAction: "Отмена")
    }
}
