//
//  MainPresenterProtocol.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

protocol MainPresenterProtocol: AnyObject {
    
    func initialLoad()
    
    func editSkillList(_ disableEditor: Bool)
    
    func deleteItem(index: Int)
    
    func appendItem(item: String?) 
   
    func showAlert()
}
