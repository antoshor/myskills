//
//  MainViewProtocol.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

protocol MainViewProtocol: AnyObject {
    
    func showUI(with data: MainModel, disableEditor: Bool, animating: Bool)
    
    func reloadSkillList(with skillList: MainModel?, disableEditor: Bool)
    
    func showEditAlert(title: String, subTitle: String, acceptAction: String, cancelAction: String)
    
    func showDuplicateAlert(title: String, subTitle: String, acceptAction: String)
}
