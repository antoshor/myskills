//
//  ViewController.swift
//  MySkills
//
//  Created by Mac Admin on 20.10.2023.
//

import UIKit

class MainViewController: UIViewController {
    
    // MARK: - UI Properties
    private lazy var mainCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: MainCompositionalLayout.layout.createLayout())
        
        collectionView.register(ProfileCell.self, forCellWithReuseIdentifier: ProfileCell.reuseID)
        collectionView.register(SkillsCell.self, forCellWithReuseIdentifier: SkillsCell.reuseID)
        collectionView.register(EditCell.self, forCellWithReuseIdentifier: EditCell.reuseID)
        collectionView.register(InfoCell.self, forCellWithReuseIdentifier: InfoCell.reuseID)
        
        
        return collectionView
    }()
    
    // MARK: - Public Properties
    var presenter: MainPresenterProtocol?
    var dataSource: MainDiffableDataSourceProtocol?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = #colorLiteral(red: 0.9625456929, green: 0.9627652764, blue: 0.968842566, alpha: 1)
        setupConstraints()
        presenter?.initialLoad()
        title = "Профиль"
    }
    
    // MARK: - Private Methods
    private func setupConstraints() {
        mainCollectionView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(mainCollectionView)
        
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            mainCollectionView.topAnchor.constraint(equalTo: safeArea.topAnchor),
            mainCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            mainCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            mainCollectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }
}

// MARK: - MainViewProtocol
extension MainViewController: MainViewProtocol {
    // MARK: - showEditAlert
    func showEditAlert(title: String, subTitle: String, acceptAction: String, cancelAction: String) {
        
        let alert = UIAlertController(title: title, message: subTitle, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: cancelAction, style: .default, handler: nil))
        
        alert.addAction(UIAlertAction(title: acceptAction, style: .default, handler: { action in
            let item =  alert.textFields?.first?.text
            self.presenter?.appendItem(item: item)
        }))
        
        alert.addTextField(configurationHandler: nil)
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - showDuplicateAlert
    func showDuplicateAlert(title: String, subTitle: String, acceptAction: String) {
        
        let alert = UIAlertController(title: title, message: subTitle, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: acceptAction, style: .default, handler: nil))
        
        present(alert, animated: true, completion: nil)
    }
    
    // MARK: - reloadSkillList
    func reloadSkillList(with skillList: MainModel?, disableEditor: Bool) {
        dataSource?.reloadData(with: skillList, disableEditor: disableEditor, isAnimating: true)
    }
    
    // MARK: - showUI
    func showUI(with data: MainModel, disableEditor: Bool, animating: Bool) {
        dataSource?.configure(collectionView: mainCollectionView)
        dataSource?.reloadData(with: data, disableEditor: disableEditor, isAnimating: animating)
        
        dataSource?.isEditCallback = { [weak self] disableEditor in
            self?.presenter?.editSkillList(disableEditor)
        }
        
        dataSource?.deleteItemFromListCallback = { [weak self] itemIndex in
            self?.presenter?.deleteItem(index: itemIndex)
        }
        
        dataSource?.addItemToListCallback = { [weak self] in
            self?.presenter?.showAlert()
        }
    }
}
