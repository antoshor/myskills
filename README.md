# Мои навыки
Это iOS приложение, которое отображает резюме iOS разработчика. 

Экран делится на 3 секции с использованием CompositionalLayout и DiffableDataSource. 
Есть раздел для отображения навыков,
можно редактировать навыки путем добавления или удаления их. 

## Стек
- Архитектура: MVP
- Верстка кодом: UIKit
- Хранение данных: CoreData

## Демо 

<img src="MySkills/Resources/Assets.xcassets/Demo.dataset/Demo.gif" width="340" height="700"/>


## Скриншоты 
<img src="Images/Img1.png" width="300" height="630"/>
<img src="Images/Img2.png" width="300" height="630"/>
<img src="Images/Img3.png" width="300" height="630"/>
<img src="Images/Img4.png" width="300" height="630"/>
<img src="Images/Img5.png" width="300" height="630"/>
